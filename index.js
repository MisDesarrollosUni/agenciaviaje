import express from "express";
import router from "./routes/index.js";
import db from "./config/db.js";

// ? Creamos una instancia
const app = express();

// ? Conectar la base de datos
db.authenticate()
    .then(() => console.log("Base de datos conectada"))
    .catch(() => console.error("Error en la conexión"));

// ? Agregamos el puerto
const port = process.env.PORT || 4000;

// ? Habilitar pug
app.set("view engine", "pug");

// ? Obtener año actual
app.use((req, res, next) => {
    // console.log(res);
    // * Variables que existen en toda la página
    res.locals.actualYear = new Date().getFullYear();
    res.locals.nombreSitio = "Agencia de viajes";
    // console.log(res.locals)
    return next();
});

// ? Definir la carpeta pública
app.use(express.static("public"));

// ? Agregar Router
app.use("/", router);

// ? Iniciamos el puerto
app.listen(port, () => {
    console.log(`El servidor se inicio en el puerto ${port}`);
});
