import express from "express";
import {
    paginaInicio,
    paginaNosotros,
    paginaViajes,
    paginaTestimoniales,
    paginaDetalleViaje
} from "../controllers/paginasController.js";
const router = express.Router();

// ? Definimos las rutas y las asignamos a router
router.get("/", paginaInicio);

router.get("/nosotros", paginaNosotros);

router.get("/viajes", paginaViajes);

router.get("/viaje/:slug", paginaDetalleViaje);

router.get("/testimoniales", paginaTestimoniales);

export default router;
