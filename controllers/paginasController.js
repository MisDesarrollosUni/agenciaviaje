import { Viaje } from "../models/Viaje.js";

const paginaInicio = (req, res) => {
    // ? Envia el archivo nosotros.pug de nuestra carpeta vistas con render
    res.render("inicio", {
        pagina: "Inicio",
    });
};

const paginaNosotros = (req, res) => {
    res.render("nosotros", {
        pagina: "Nosotros",
    });
};

const paginaViajes = async (req, res) => {
    // * Consultar base de datos
    const viajes = await Viaje.findAll();
    res.render("viajes", {
        pagina: "Viajes",
        viajes,
    });
};

const paginaTestimoniales = (req, res) => {
    res.render("testimoniales", {
        pagina: "Testimoniales",
    });
};

const paginaDetalleViaje = async (req, res) => {
    const { slug } = req.params;
    try {
        const viaje = await Viaje.findOne({ where: { slug } });
        res.render("viaje", {
            pagina: "Información viaje",
            viaje,
        });
    } catch (error) {
        console.error(error);
    }
};

export {
    paginaInicio,
    paginaNosotros,
    paginaViajes,
    paginaTestimoniales,
    paginaDetalleViaje,
};
